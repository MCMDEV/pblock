package de.mcmdev.pblock.registry;

import de.mcmdev.pblock.api.Action;

import java.util.ArrayList;
import java.util.List;

public class ActionRegistry {

    private List<Action> actions = new ArrayList<>();

    public List<Action> getActions() {
        return this.actions;
    }

    public void addAction(Action action) {
        this.actions.add(action);
    }


}
