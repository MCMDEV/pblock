package de.mcmdev.pblock.registry;

import de.mcmdev.pblock.PBlock;
import de.mcmdev.pblock.api.CommandBlock;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CommandBlockRegistry {

    private PBlock plugin;

    public CommandBlockRegistry(PBlock plugin) {
        this.plugin = plugin;
    }

    private List<CommandBlock> commandBlocks = new ArrayList<>();

    public void addCommandBlock(CommandBlock commandBlock) {
        this.commandBlocks.add(commandBlock);
    }

    public void updateCommandBlock(CommandBlock commandBlock)   {
        Optional<CommandBlock> optionalCommandBlock = getCommandBlockAtLocation(commandBlock.getLocation());
        if(optionalCommandBlock.isPresent())    {
            this.commandBlocks.remove(commandBlock);
            addCommandBlock(commandBlock);
        }   else    {
            addCommandBlock(commandBlock);
        }
    }

    public void removeCommandBlock(CommandBlock commandBlock) {
        this.commandBlocks.remove(commandBlock);
    }

    public Optional<CommandBlock> getCommandBlockAtLocation(Location location) {
        return commandBlocks.stream().filter(commandBlock -> commandBlock.getLocation().distanceSquared(location) < 1).findFirst();
    }

    public void saveAll() {
        for (CommandBlock commandBlock : this.commandBlocks) {
            this.plugin.getConfig().set(String.valueOf(this.commandBlocks.indexOf(commandBlock)), commandBlock);
        }
        this.plugin.saveConfig();
    }

    public void loadAll() {
        for (String key : plugin.getConfig().getKeys(false)) {
            CommandBlock commandBlock = (CommandBlock) this.plugin.getConfig().get(key);
            addCommandBlock(commandBlock);
        }
    }


}
