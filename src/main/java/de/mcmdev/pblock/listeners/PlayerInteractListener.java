package de.mcmdev.pblock.listeners;

import de.mcmdev.pblock.PBlock;
import de.mcmdev.pblock.api.CommandBlock;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.Optional;

public class PlayerInteractListener implements Listener {

    private PBlock plugin;

    public PlayerInteractListener(PBlock plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK && event.getClickedBlock().getType() == Material.PRISMARINE) {
            Optional<CommandBlock> optionalCommandBlock = plugin.getCommandBlockRegistry().getCommandBlockAtLocation(event.getClickedBlock().getLocation());
            if (!optionalCommandBlock.isPresent()) {
                CommandBlock commandBlock = new CommandBlock(event.getClickedBlock().getLocation());
                commandBlock.open(plugin, event.getPlayer());
            } else {
                optionalCommandBlock.get().open(plugin, event.getPlayer());
            }
        }
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        if (event.getBlock().getType() == Material.PRISMARINE) {
            Optional<CommandBlock> optionalCommandBlock = plugin.getCommandBlockRegistry().getCommandBlockAtLocation(event.getBlock().getLocation());
            optionalCommandBlock.ifPresent(commandBlock -> plugin.getCommandBlockRegistry().removeCommandBlock(commandBlock));
        }
    }

}
