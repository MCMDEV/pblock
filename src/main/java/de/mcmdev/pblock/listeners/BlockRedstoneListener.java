package de.mcmdev.pblock.listeners;

import de.mcmdev.pblock.PBlock;
import de.mcmdev.pblock.api.CommandBlock;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockRedstoneEvent;

import java.util.Optional;

public class BlockRedstoneListener implements Listener {

    private PBlock plugin;

    public BlockRedstoneListener(PBlock plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onRedstone(BlockRedstoneEvent event) {
        for (int x = -1; x < 2; x++) {
            for (int y = -1; y < 2; y++) {
                for (int z = -1; z < 2; z++) {
                    if (event.getBlock().getWorld().getBlockAt(event.getBlock().getLocation().getBlockX() + x, event.getBlock().getLocation().getBlockY() + y, event.getBlock().getLocation().getBlockZ() + z).getType() == Material.PRISMARINE) {
                        Block prismarine = event.getBlock().getWorld().getBlockAt(event.getBlock().getLocation().getBlockX() + x, event.getBlock().getLocation().getBlockY() + y, event.getBlock().getLocation().getBlockZ() + z);

                        if(!prismarine.isBlockPowered()) {
                            Optional<CommandBlock> optionalCommandBlock = plugin.getCommandBlockRegistry().getCommandBlockAtLocation(prismarine.getLocation());
                            optionalCommandBlock.ifPresent(commandBlock -> {
                                commandBlock.execute(plugin, commandBlock);
                            });

                        }
                    }
                }
            }
        }
    }

}
