package de.mcmdev.pblock;

import de.mcmdev.pblock.api.CommandBlock;
import de.mcmdev.pblock.api.impl.actions.*;
import de.mcmdev.pblock.api.impl.parameters.*;
import de.mcmdev.pblock.listeners.BlockRedstoneListener;
import de.mcmdev.pblock.listeners.PlayerInteractListener;
import de.mcmdev.pblock.registry.ActionRegistry;
import de.mcmdev.pblock.registry.CommandBlockRegistry;
import fr.minuskube.inv.InventoryManager;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.plugin.java.JavaPlugin;

public final class PBlock extends JavaPlugin {

    private InventoryManager manager;
    private CommandBlockRegistry commandBlockRegistry;
    private ActionRegistry actionRegistry;

    @Override
    public void onEnable() {
        registerSerializers();

        commandBlockRegistry = new CommandBlockRegistry(this);
        actionRegistry = new ActionRegistry();
        actionRegistry.addAction(new TeleportAction());
        actionRegistry.addAction(new GiveItemAction());
        actionRegistry.addAction(new SetHealthAction());
        actionRegistry.addAction(new SetFoodAction());
        actionRegistry.addAction(new MobSpawnAction());
        actionRegistry.addAction(new RevokeItemAction());
        actionRegistry.addAction(new SetBlockAction());
        actionRegistry.addAction(new ClearPotionAction());
        actionRegistry.addAction(new AddPotionAction());

        manager = new InventoryManager(this);
        manager.init();

        getServer().getPluginManager().registerEvents(new PlayerInteractListener(this), this);
        getServer().getPluginManager().registerEvents(new BlockRedstoneListener(this), this);

        commandBlockRegistry.loadAll();
    }

    @Override
    public void onDisable() {
        commandBlockRegistry.saveAll();
    }

    private void registerSerializers() {
        ConfigurationSerialization.registerClass(CommandBlock.class);

        ConfigurationSerialization.registerClass(TeleportAction.class);
        ConfigurationSerialization.registerClass(GiveItemAction.class);
        ConfigurationSerialization.registerClass(SetHealthAction.class);
        ConfigurationSerialization.registerClass(SetFoodAction.class);
        ConfigurationSerialization.registerClass(MobSpawnAction.class);
        ConfigurationSerialization.registerClass(RevokeItemAction.class);
        ConfigurationSerialization.registerClass(SetBlockAction.class);
        ConfigurationSerialization.registerClass(AddPotionAction.class);
        ConfigurationSerialization.registerClass(ClearPotionAction.class);

        ConfigurationSerialization.registerClass(LocationParameter.class);
        ConfigurationSerialization.registerClass(PlayersParameter.class);
        ConfigurationSerialization.registerClass(NumberParameter.class);
        ConfigurationSerialization.registerClass(ItemStackParameter.class);
        ConfigurationSerialization.registerClass(MobsParameter.class);
        ConfigurationSerialization.registerClass(EffectParameter.class);
    }

    public InventoryManager getManager() {
        return manager;
    }

    public ActionRegistry getActionRegistry() {
        return actionRegistry;
    }

    public CommandBlockRegistry getCommandBlockRegistry() {
        return commandBlockRegistry;
    }
}
