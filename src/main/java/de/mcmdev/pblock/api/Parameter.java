package de.mcmdev.pblock.api;

import de.mcmdev.pblock.PBlock;
import fr.minuskube.inv.SmartInventory;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public abstract class Parameter<T> {

    private String name;
    private Material material;
    private T object;

    public Parameter(String name, Material material) {
        this.name = name;
        this.material = material;
    }

    public T getObject() {
        return object;
    }

    public String getName() {
        return name;
    }

    public Material getMaterial() {
        return material;
    }

    public void setObject(T object) {
        this.object = object;
    }

    public abstract String getObjectString();

    public abstract void openInGui(Player player, SmartInventory parent, PBlock plugin);

    public abstract Object receive(PBlock plugin, CommandBlock commandBlock);

    @Override
    public String toString() {
        return "Parameter{" +
                "name='" + name + '\'' +
                ", material=" + material +
                ", object=" + object +
                '}';
    }
}
