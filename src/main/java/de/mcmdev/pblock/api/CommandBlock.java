package de.mcmdev.pblock.api;

import de.mcmdev.pblock.PBlock;
import de.mcmdev.pblock.api.impl.inventories.CommandBlockInv;
import fr.minuskube.inv.SmartInventory;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class CommandBlock implements ConfigurationSerializable {

    private Location location;
    private Action action;

    public CommandBlock(Location location) {
        this.location = location;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public Location getLocation() {
        return location;
    }

    public Action getAction() {
        return action;
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();
        map.put("location", location);
        map.put("action", action);
        return map;
    }

    public static CommandBlock deserialize(Map<String, Object> map)  {
        CommandBlock commandBlock = new CommandBlock((Location) map.get("location"));
        commandBlock.setAction((Action) map.get("action"));
        return commandBlock;
    }

    public void execute(PBlock plugin, CommandBlock commandBlock)    {
        action.execute(commandBlock, plugin);
    }

    public void open(PBlock plugin, Player player)  {
        SmartInventory inventory = new CommandBlockInv(plugin, player, this).getInventory();
        inventory.open(player);
    }

    @Override
    public String toString() {
        return "CommandBlock{" +
                "location=" + location +
                ", action=" + action +
                '}';
    }
}
