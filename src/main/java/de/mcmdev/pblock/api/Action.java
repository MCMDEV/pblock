package de.mcmdev.pblock.api;

import de.mcmdev.pblock.PBlock;
import javafx.collections.transformation.SortedList;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class Action {

    private String name;
    private List<String> parameterNames = new ArrayList<>();
    private List<Parameter> parameters = new ArrayList<>();
    private Material material;

    public Action(String name, Material material) {
        this.name = name;
        this.material = material;
    }

    public abstract void execute(CommandBlock commandBlock, PBlock plugin);

    public void setName(String name) {
        this.name = name;
    }

    public void addParameter(Parameter parameter)   {
        this.parameters.add(parameter);
        this.parameterNames.add("_" + parameters.size());
    }

    public void addParameter(String name, Parameter parameter)   {
        this.parameters.add(parameter);
        this.parameterNames.add(name);
    }

/*    public void setParameters(List<Parameter> parameters, String... parameterNames) {
        HashMap<String, Parameter> newMap = new HashMap<>();
        for(int i = 0; i < parameters.size(); i++)    {
            if(parameterNames.length >= i) {
                Parameter parameter = parameters.get(i);
                String parameterName = parameterNames[i];
                newMap.put(parameterName, parameter);
            }
        }
        this.parameters = newMap;
    }*/

    public void setParameters(List<Parameter> parameters) {
        this.parameters = parameters;
    }

    public String getName() {
        return name;
    }

    public Optional<Parameter> getFirstParameterOfType(Class clazz)   {
        return this.parameters.stream().filter(parameter -> parameter.getClass().equals(clazz)).findFirst();
    }

    public <T extends Parameter> List<T> getAllParameterOfType(Class<T> clazz)   {
        return this.parameters.stream().filter(parameter -> parameter.getClass().equals(clazz)).map(parameter -> (T) parameter).collect(Collectors.toList());
    }

    public List<Parameter> getParameters() {
        return this.parameters;
    }

    public HashMap<String, Parameter> getNamedParameters() {
        HashMap<String, Parameter> named = new HashMap<>();
        if(parameters.size() != parameterNames.size())  {
            throw new IllegalStateException("Amount of parameter names is not equal to amount of parameters");
        }
        for (int i = 0; i < parameters.size(); i++) {
            named.put(parameterNames.get(i), parameters.get(i));
        }
        return named;
    }

    public Material getMaterial() {
        return material;
    }

    @Override
    public String toString() {
        return "Action{" +
                "name='" + name + '\'' +
                ", parameterNames=" + parameterNames +
                ", parameters=" + parameters +
                ", material=" + material +
                '}';
    }
}
