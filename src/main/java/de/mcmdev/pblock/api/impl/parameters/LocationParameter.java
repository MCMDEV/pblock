package de.mcmdev.pblock.api.impl.parameters;

import de.mcmdev.pblock.PBlock;
import de.mcmdev.pblock.api.CommandBlock;
import de.mcmdev.pblock.api.Parameter;
import fr.minuskube.inv.SmartInventory;
import net.wesjd.anvilgui.AnvilGUI;
import org.apache.commons.lang.math.NumberUtils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LocationParameter extends Parameter<Location> implements ConfigurationSerializable {

    public LocationParameter() {
        super("§bPosition", Material.EYE_OF_ENDER);
    }

    @Override
    public String getObjectString() {
        Location location = getObject();
        return location.getX() + " " + location.getY() + " " + location.getZ();
    }

    @Override
    public void openInGui(Player player, SmartInventory parent, PBlock plugin) {
        new AnvilGUI.Builder().text("Koordinaten eingeben")
                .onComplete((player1, s) -> {
                    String[] args = s.split(" ");
                    String x = args[0];
                    String y = args[1];
                    String z = args[2];
                    if (NumberUtils.isNumber(x) && NumberUtils.isNumber(y) && NumberUtils.isNumber(z)) {
                        Location location = new Location(player.getWorld(), Double.parseDouble(x), Double.parseDouble(y), Double.parseDouble(z));
                        setObject(location);
                        parent.open(player);
                        return AnvilGUI.Response.close();
                    }
                    return AnvilGUI.Response.text("Koordinaten eingeben");


                })
                .plugin(plugin)
                .open(player);
    }

    @Override
    public Location receive(PBlock plugin, CommandBlock commandBlock) {
        return getObject();
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();
        map.put("location", getObject());
        return map;
    }

    public static LocationParameter deserialize(Map<String, Object> map) {
        LocationParameter locationParameter = new LocationParameter();
        locationParameter.setObject((Location) map.get("location"));
        return locationParameter;
    }
}
