package de.mcmdev.pblock.api.impl.actions;

import de.mcmdev.pblock.PBlock;
import de.mcmdev.pblock.api.Action;
import de.mcmdev.pblock.api.CommandBlock;
import de.mcmdev.pblock.api.Parameter;
import de.mcmdev.pblock.api.impl.parameters.LocationParameter;
import de.mcmdev.pblock.api.impl.parameters.PlayersParameter;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class TeleportAction extends Action implements ConfigurationSerializable {

    public TeleportAction() {
        super("§5Teleportieren", Material.ENDER_PEARL);
        addParameter(new PlayersParameter());
        addParameter(new LocationParameter());
    }


    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();
        map.put("parameters", getParameters());
        return map;
    }

    public static TeleportAction deserialize(Map<String, Object> map) {
        TeleportAction teleportAction = new TeleportAction();
        teleportAction.setParameters((List<Parameter>) map.get("parameters"));
        return teleportAction;
    }

    @Override
    public void execute(CommandBlock commandBlock, PBlock plugin) {
        Location location = null;
        List<Player> players = null;

        Optional<Parameter> optionalLocation = getFirstParameterOfType(LocationParameter.class);
        if (optionalLocation.isPresent() && optionalLocation.get() instanceof LocationParameter) {
            LocationParameter parameter = (LocationParameter) optionalLocation.get();
            location = (Location) parameter.receive(plugin, commandBlock);
        }

        Optional<Parameter> optionalPlayers = getFirstParameterOfType(PlayersParameter.class);
        if (optionalPlayers.isPresent() && optionalPlayers.get() instanceof PlayersParameter) {
            PlayersParameter parameter = (PlayersParameter) optionalPlayers.get();
            players = (List<Player>) parameter.receive(plugin, commandBlock);
        }

        for (Player player : players) {
            player.teleport(location);
        }
    }
}
