package de.mcmdev.pblock.api.impl.actions;

import de.mcmdev.pblock.PBlock;
import de.mcmdev.pblock.api.Action;
import de.mcmdev.pblock.api.CommandBlock;
import de.mcmdev.pblock.api.Parameter;
import de.mcmdev.pblock.api.impl.parameters.PlayersParameter;
import org.bukkit.Material;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class ClearPotionAction extends Action implements ConfigurationSerializable {

    public ClearPotionAction() {
        super("§fEffekte entfernen", Material.MILK_BUCKET);
        addParameter(new PlayersParameter());
    }

    @Override
    public void execute(CommandBlock commandBlock, PBlock plugin) {
        List<Player> players = null;

        Optional<Parameter> optionalPlayers = getFirstParameterOfType(PlayersParameter.class);
        if (optionalPlayers.isPresent() && optionalPlayers.get() instanceof PlayersParameter) {
            PlayersParameter parameter = (PlayersParameter) optionalPlayers.get();
            players = (List<Player>) parameter.receive(plugin, commandBlock);
        }

        for (Player player : players) {
            player.getActivePotionEffects().forEach(potionEffect -> player.removePotionEffect(potionEffect.getType()));
        }
    }


    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();
        map.put("parameters", getParameters());
        return map;
    }

    public static ClearPotionAction deserialize(Map<String, Object> map) {
        ClearPotionAction clearPotionAction = new ClearPotionAction();
        clearPotionAction.setParameters((List<Parameter>) map.get("parameters"));
        return clearPotionAction;
    }
}
