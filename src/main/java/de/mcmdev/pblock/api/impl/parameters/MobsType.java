package de.mcmdev.pblock.api.impl.parameters;

import org.bukkit.entity.EntityType;

public class MobsType {

    private String typeId;
    private String name;
    private EntityType entityType;

    public MobsType(String typeId, String name) {
        this.typeId = typeId;
        this.name = name;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }
}
