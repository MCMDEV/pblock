package de.mcmdev.pblock.api.impl.inventories;

import de.mcmdev.pblock.PBlock;
import de.mcmdev.pblock.api.CommandBlock;
import de.mcmdev.pblock.api.Parameter;
import de.mcmdev.pblock.utils.ItemStackBuilder;
import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.InventoryListener;
import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class CommandBlockInv {

    private PBlock plugin;
    private Player player;
    private CommandBlock commandBlock;

    public CommandBlockInv(PBlock plugin, Player player, CommandBlock commandBlock) {
        this.plugin = plugin;
        this.player = player;
        this.commandBlock = commandBlock;

        load();
    }

    private SmartInventory inventory;
    private CommandBlock backupState;

    private void load() {
        backupState = commandBlock;
        inventory = SmartInventory.builder()
                .title("§3PBlock")
                .manager(plugin.getManager())
                .size(6, 9)
                .id("pblock_commandblock")
                .provider(new InventoryProvider() {
                    @Override
                    public void init(Player player, InventoryContents contents) {
                        if(commandBlock.getAction() == null)    {
                            contents.set(2, 4, ClickableItem.of(

                                    ItemStackBuilder.create(Material.STAINED_GLASS_PANE)
                                            .withColor(ItemStackBuilder.BlockColor.RED)
                                            .withName("§7Aktion festlegen")
                                            .withLore("§cEs wurde noch keine Aktion festgelegt")
                                            .build()

                                    , new Consumer<InventoryClickEvent>() {
                                        @Override
                                        public void accept(InventoryClickEvent event) {
                                            ActionSetInv actionSetInv = new ActionSetInv(plugin, inventory, commandBlock);
                                            actionSetInv.getInventory().open(player);
                                        }
                                    }));
                        }   else    {
                            contents.set(2, 4, ClickableItem.of(

                                    ItemStackBuilder.create(commandBlock.getAction().getMaterial())
                                            .withName(commandBlock.getAction().getName())
                                            .withLore("§aAktion überschreiben")
                                            .build()

                                    , new Consumer<InventoryClickEvent>() {
                                        @Override
                                        public void accept(InventoryClickEvent event) {
                                            ActionSetInv actionSetInv = new ActionSetInv(plugin, inventory, commandBlock);
                                            actionSetInv.getInventory().open(player);
                                        }
                                    }));

                            HashMap<String, Parameter> parameters = commandBlock.getAction().getNamedParameters();
                            boolean foundNull = false;
                            int lastIndex = 0;
                            for(Map.Entry<String, Parameter> namedParameter : parameters.entrySet())   {
                                Parameter parameter = namedParameter.getValue();
                                if(parameter.getObject() == null)   {
                                    contents.set(3, lastIndex, ClickableItem.of(

                                            ItemStackBuilder.create(Material.STAINED_GLASS_PANE)
                                                    .withColor(ItemStackBuilder.BlockColor.RED)
                                                    .withName(namedParameter.getKey().startsWith("_") ?
                                                            parameter.getName() : parameter.getName() + " §e(" + namedParameter.getKey() + ")")
                                                    .withLore("§cNoch nicht festgelegt", "§7Klicke hier um den Parameter festzulegen")
                                                    .build()

                                            , new Consumer<InventoryClickEvent>() {
                                                @Override
                                                public void accept(InventoryClickEvent event) {
                                                    parameter.openInGui(player, inventory, plugin);
                                                }
                                            }));
                                    foundNull = true;
                                }   else    {
                                    contents.set(3, lastIndex, ClickableItem.of(

                                            ItemStackBuilder.create(parameter.getMaterial())
                                                    .withName(parameter.getName())
                                                    .withLore("§7" + parameter.getObjectString(), "§7Klicke hier um den Parameter festzulegen")
                                                    .build()

                                            , new Consumer<InventoryClickEvent>() {
                                                @Override
                                                public void accept(InventoryClickEvent event) {
                                                    parameter.openInGui(player, inventory, plugin);
                                                }
                                            }));
                                }
                                lastIndex++;
                            }

                            if(foundNull)   {
                                contents.fillRow(0, ClickableItem.of(

                                        ItemStackBuilder.create(Material.STAINED_GLASS_PANE)
                                                .withColor(ItemStackBuilder.BlockColor.RED)
                                                .withName("§cAbbrechen und verlasssen")
                                                .withLore("§cEs fehlen noch Parameter oder eine Aktion")
                                                .build()

                                        , new Consumer<InventoryClickEvent>() {
                                            @Override
                                            public void accept(InventoryClickEvent event) {
                                                inventory.close(player);
                                            }
                                        }));
                            }   else    {
                                contents.fillRow(0, ClickableItem.of(

                                        ItemStackBuilder.create(Material.STAINED_GLASS_PANE)
                                                .withColor(ItemStackBuilder.BlockColor.LIME)
                                                .withName("§aSpeichern und verlasssen")
                                                .withLore("§aMuss beim Speichern ausgeführt werden")
                                                .build()

                                        , new Consumer<InventoryClickEvent>() {
                                            @Override
                                            public void accept(InventoryClickEvent event) {
                                                plugin.getCommandBlockRegistry().updateCommandBlock(commandBlock);
                                                inventory.close(player);
                                            }
                                        }));
                            }

                        }

                    }

                    @Override
                    public void update(Player player, InventoryContents contents) {

                    }
                }).listener(new InventoryListener<>(InventoryCloseEvent.class, new Consumer<InventoryCloseEvent>() {
                    @Override
                    public void accept(InventoryCloseEvent inventoryCloseEvent) {
                        commandBlock = backupState;
                    }
                })).build();
    }

    public SmartInventory getInventory() {
        return inventory;
    }
}
