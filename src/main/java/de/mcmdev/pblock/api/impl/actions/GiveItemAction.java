package de.mcmdev.pblock.api.impl.actions;

import de.mcmdev.pblock.PBlock;
import de.mcmdev.pblock.api.Action;
import de.mcmdev.pblock.api.CommandBlock;
import de.mcmdev.pblock.api.Parameter;
import de.mcmdev.pblock.api.impl.parameters.ItemStackParameter;
import de.mcmdev.pblock.api.impl.parameters.LocationParameter;
import de.mcmdev.pblock.api.impl.parameters.PlayersParameter;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class GiveItemAction extends Action implements ConfigurationSerializable {

    public GiveItemAction() {
        super("§eItem geben", Material.GRASS);
        addParameter(new PlayersParameter());
        addParameter(new ItemStackParameter());
    }


    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();
        map.put("parameters", getParameters());
        return map;
    }

    public static GiveItemAction deserialize(Map<String, Object> map) {
        GiveItemAction teleportAction = new GiveItemAction();
        teleportAction.setParameters((List<Parameter>) map.get("parameters"));
        return teleportAction;
    }

    @Override
    public void execute(CommandBlock commandBlock, PBlock plugin) {
        ItemStack itemStack = null;
        List<Player> players = null;

        Optional<Parameter> optionalItemStack = getFirstParameterOfType(ItemStackParameter.class);
        if (optionalItemStack.isPresent() && optionalItemStack.get() instanceof ItemStackParameter) {
            ItemStackParameter parameter = (ItemStackParameter) optionalItemStack.get();
            itemStack = (ItemStack) parameter.receive(plugin, commandBlock);
        }

        Optional<Parameter> optionalPlayers = getFirstParameterOfType(PlayersParameter.class);
        if (optionalPlayers.isPresent() && optionalPlayers.get() instanceof PlayersParameter) {
            PlayersParameter parameter = (PlayersParameter) optionalPlayers.get();
            players = (List<Player>) parameter.receive(plugin, commandBlock);
        }

        for (Player player : players) {
            player.getInventory().addItem(itemStack);
        }
    }
}
