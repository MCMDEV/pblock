package de.mcmdev.pblock.api.impl.actions;

import de.mcmdev.pblock.PBlock;
import de.mcmdev.pblock.api.Action;
import de.mcmdev.pblock.api.CommandBlock;
import de.mcmdev.pblock.api.Parameter;
import de.mcmdev.pblock.api.impl.parameters.LocationParameter;
import de.mcmdev.pblock.api.impl.parameters.MobsParameter;
import de.mcmdev.pblock.api.impl.parameters.MobsType;
import de.mcmdev.pblock.api.impl.parameters.PlayersParameter;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.util.*;

public class MobSpawnAction extends Action implements ConfigurationSerializable {

    public MobSpawnAction() {
        super("§3Kreatur erschaffen", Material.MOB_SPAWNER);
        addParameter(new MobsParameter());
        addParameter(new LocationParameter());
    }


    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();
        map.put("parameters", getParameters());
        return map;
    }

    public static MobSpawnAction deserialize(Map<String, Object> map) {
        MobSpawnAction mobSpawnAction = new MobSpawnAction();
        mobSpawnAction.setParameters((List<Parameter>) map.get("parameters"));
        return mobSpawnAction;
    }

    @Override
    public void execute(CommandBlock commandBlock, PBlock plugin) {
        Location location = null;
        MobsType mobsType = null;


        Optional<Parameter> optionalLocation = getFirstParameterOfType(LocationParameter.class);
        if (optionalLocation.isPresent() && optionalLocation.get() instanceof LocationParameter) {
            LocationParameter parameter = (LocationParameter) optionalLocation.get();
            location = (Location) parameter.receive(plugin, commandBlock);
        }

        Optional<Parameter> optionalMobsType = getFirstParameterOfType(MobsParameter.class);
        if (optionalMobsType.isPresent() && optionalMobsType.get() instanceof MobsParameter) {
            MobsParameter parameter = (MobsParameter) optionalMobsType.get();
            mobsType = (MobsType) parameter.getObject();
        }

        if (mobsType.getTypeId().equals("type_mobs")) {
            location.getWorld().spawnEntity(location, mobsType.getEntityType());
        }

        if(mobsType.getTypeId().equalsIgnoreCase("all_mobs")) {
            if(location == null)
                return;
            for (EntityType value : EntityType.values()) {
                if(value == EntityType.DROPPED_ITEM)
                    continue;
                if(value == EntityType.LEASH_HITCH)
                    continue;
                if(value == EntityType.PAINTING)
                    continue;
                if(value == EntityType.ENDER_PEARL)
                    continue;
                if(value == EntityType.ITEM_FRAME)
                    continue;
                if(value == EntityType.FISHING_HOOK)
                    continue;
                if(value == EntityType.WEATHER)
                    continue;
                if(value == EntityType.PLAYER)
                    continue;
                if(value == EntityType.COMPLEX_PART)
                    continue;
                if(value == EntityType.UNKNOWN)
                    continue;
                System.out.println(location);
                System.out.println(value);
                System.out.println(location.getWorld());
                location
                        .getWorld()
                        .spawnEntity(location,
                        value);
            }
        }
    }
}
