package de.mcmdev.pblock.api.impl.parameters;

import de.mcmdev.pblock.PBlock;
import de.mcmdev.pblock.api.CommandBlock;
import de.mcmdev.pblock.api.Parameter;
import de.mcmdev.pblock.utils.ItemStackBuilder;
import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;
import org.bukkit.Material;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class NumberParameter extends Parameter<Integer> implements ConfigurationSerializable {

    private int min;
    private int max;

    public NumberParameter() {
        super("§9Number", Material.REDSTONE_TORCH_ON);
    }

    public NumberParameter(int min, int max) {
        super("§9Number", Material.REDSTONE_TORCH_ON);
        this.min = min;
        this.max = max;
    }

    @Override
    public String getObjectString() {
        return getObject().toString();
    }

    @Override
    public void openInGui(Player player, SmartInventory parent, PBlock plugin) {
        SmartInventory inventory = SmartInventory.builder()
                .id("pblock_parameter_number")
                .type(InventoryType.HOPPER)
                .manager(plugin.getManager())
                .title("§9Nummer festlegen")
                .provider(new InventoryProvider() {
                    @Override
                    public void init(Player player, InventoryContents contents) {
                        if(getObject() == null) {
                            setObject(0);
                        }
                        contents.set(0, 0, ClickableItem.of(

                                ItemStackBuilder.create(Material.STAINED_GLASS_PANE)
                                        .withColor(ItemStackBuilder.BlockColor.BLUE)
                                        .withName("§c-1")
                                        .build()

                                , new Consumer<InventoryClickEvent>() {
                                    @Override
                                    public void accept(InventoryClickEvent event) {
                                        if(getObject() - 1 >= min)    {
                                            setObject(getObject() - 1);
                                        }
                                    }
                                }));

                        contents.set(0, 4, ClickableItem.of(

                                ItemStackBuilder.create(Material.STAINED_GLASS_PANE)
                                        .withColor(ItemStackBuilder.BlockColor.BLUE)
                                        .withName("§a+1")
                                        .build()

                                , new Consumer<InventoryClickEvent>() {
                                    @Override
                                    public void accept(InventoryClickEvent event) {
                                        if(getObject() + 1 <= max)    {
                                            setObject(getObject() + 1);
                                        }
                                    }
                                }));
                    }

                    @Override
                    public void update(Player player, InventoryContents contents) {
                        contents.set(0, 2, ClickableItem.of(

                                ItemStackBuilder.create(Material.STAINED_GLASS_PANE)
                                        .withColor(ItemStackBuilder.BlockColor.BLUE)
                                        .withName("§9Nummer: " + getObject())
                                        .build()

                                , new Consumer<InventoryClickEvent>() {
                                    @Override
                                    public void accept(InventoryClickEvent event) {
                                        parent.open(player);
                                    }
                                }));

                    }
                }).build();
        inventory.open(player);
    }

    @Override
    public Integer receive(PBlock plugin, CommandBlock commandBlock) {
        return getObject();
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();
        map.put("number", getObject());
        return map;
    }

    public static NumberParameter deserialize(Map<String, Object> map)  {
        NumberParameter itemStackParameter = new NumberParameter();
        itemStackParameter.setObject((Integer) map.get("number"));
        return itemStackParameter;
    }
}
