package de.mcmdev.pblock.api.impl.parameters;

import de.mcmdev.pblock.PBlock;
import de.mcmdev.pblock.api.CommandBlock;
import de.mcmdev.pblock.api.Parameter;
import de.mcmdev.pblock.utils.ItemStackBuilder;
import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;
import net.wesjd.anvilgui.AnvilGUI;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

import java.util.*;
import java.util.function.Consumer;

public class ItemStackParameter extends Parameter<ItemStack> implements ConfigurationSerializable {

    public ItemStackParameter() {
        super("§1Gegenstand", Material.GRASS);
    }

    @Override
    public String getObjectString() {
        return getObject().getType().name();
    }

    @Override
    public void openInGui(Player player, SmartInventory parent, PBlock plugin) {
        SmartInventory inventory = SmartInventory.builder()
                .id("pblock_parameter_itemstack")
                .type(InventoryType.HOPPER)
                .manager(plugin.getManager())
                .title("§3Gegenstand auswählen")
                .provider(new InventoryProvider() {
                    @Override
                    public void init(Player player, InventoryContents contents) {
                        contents.set(0, 2, ClickableItem.of(

                                ItemStackBuilder.create(Material.STAINED_GLASS_PANE)
                                        .withColor(ItemStackBuilder.BlockColor.BLUE)
                                        .withName("§9Lege das Item hier hin")
                                        .build()

                                , event -> {
                                    setObject(event.getCursor());
                                    event.setCursor(null);
                                    parent.open(player);
                                }));
                    }

                    @Override
                    public void update(Player player, InventoryContents contents) {

                    }
                }).build();
        inventory.open(player);
    }

    @Override
    public ItemStack receive(PBlock plugin, CommandBlock commandBlock) {
        return getObject();
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();
        map.put("itemStack", getObject());
        return map;
    }

    public static ItemStackParameter deserialize(Map<String, Object> map) {
        ItemStackParameter itemStackParameter = new ItemStackParameter();
        itemStackParameter.setObject((ItemStack) map.get("itemStack"));
        return itemStackParameter;
    }
}
