package de.mcmdev.pblock.api.impl.actions;

import de.mcmdev.pblock.PBlock;
import de.mcmdev.pblock.api.Action;
import de.mcmdev.pblock.api.CommandBlock;
import de.mcmdev.pblock.api.Parameter;
import de.mcmdev.pblock.api.impl.parameters.LocationParameter;
import de.mcmdev.pblock.api.impl.parameters.NumberParameter;
import de.mcmdev.pblock.api.impl.parameters.PlayersParameter;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class SetHealthAction extends Action implements ConfigurationSerializable {

    public SetHealthAction() {
        super("§cLeben setzen", Material.GOLDEN_APPLE);
        addParameter(new PlayersParameter());
        addParameter(new NumberParameter(0, 20));
    }


    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();
        map.put("parameters", getParameters());
        return map;
    }

    public static SetHealthAction deserialize(Map<String, Object> map) {
        SetHealthAction healthAction = new SetHealthAction();
        healthAction.setParameters((List<Parameter>) map.get("parameters"));
        return healthAction;
    }

    @Override
    public void execute(CommandBlock commandBlock, PBlock plugin) {
        int number = 0;
        List<Player> players = null;

        Optional<Parameter> optionalNumber = getFirstParameterOfType(NumberParameter.class);
        if (optionalNumber.isPresent() && optionalNumber.get() instanceof NumberParameter) {
            NumberParameter parameter = (NumberParameter) optionalNumber.get();
            number = (Integer) parameter.receive(plugin, commandBlock);
        }

        Optional<Parameter> optionalPlayers = getFirstParameterOfType(PlayersParameter.class);
        if (optionalPlayers.isPresent() && optionalPlayers.get() instanceof PlayersParameter) {
            PlayersParameter parameter = (PlayersParameter) optionalPlayers.get();
            players = (List<Player>) parameter.receive(plugin, commandBlock);
        }

        if (players != null) {
            for (Player player : players) {
                player.setHealth(number);
            }
        }
    }
}
