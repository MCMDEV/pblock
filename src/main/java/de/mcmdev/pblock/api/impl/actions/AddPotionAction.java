package de.mcmdev.pblock.api.impl.actions;

import de.mcmdev.pblock.PBlock;
import de.mcmdev.pblock.api.Action;
import de.mcmdev.pblock.api.CommandBlock;
import de.mcmdev.pblock.api.Parameter;
import de.mcmdev.pblock.api.impl.parameters.EffectParameter;
import de.mcmdev.pblock.api.impl.parameters.NumberParameter;
import de.mcmdev.pblock.api.impl.parameters.PlayersParameter;
import org.bukkit.Material;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class AddPotionAction extends Action implements ConfigurationSerializable {
    public AddPotionAction() {
        super("§dStatuseffekt setzen", Material.POTION);
        addParameter(new PlayersParameter());
        addParameter(new EffectParameter());
        addParameter("Dauer", new NumberParameter(0, Integer.MAX_VALUE));
        addParameter("Stärke", new NumberParameter(0, 100));
    }

    @Override
    public void execute(CommandBlock commandBlock, PBlock plugin) {
        List<Player> players = null;
        PotionEffectType type = null;
        int duration = 0;
        int stenght = 0;

        List<NumberParameter> numberParameters = getAllParameterOfType(NumberParameter.class);
        if(numberParameters.size() == 2)    {
            NumberParameter durationParameter = numberParameters.get(0);
            duration = durationParameter.receive(plugin, commandBlock);

            NumberParameter strenghtParameter = numberParameters.get(0);
            stenght = strenghtParameter.receive(plugin, commandBlock);
        }

        Optional<Parameter> optionalPotionType = getFirstParameterOfType(EffectParameter.class);
        if(optionalPotionType.isPresent() && optionalPotionType.get() instanceof EffectParameter) {
            EffectParameter parameter = (EffectParameter) optionalPotionType.get();
            type = (PotionEffectType) parameter.receive(plugin, commandBlock);
        }

        Optional<Parameter> optionalPlayers = getFirstParameterOfType(PlayersParameter.class);
        if(optionalPlayers.isPresent() && optionalPlayers.get() instanceof PlayersParameter) {
            PlayersParameter parameter = (PlayersParameter) optionalPlayers.get();
            players = (List<Player>) parameter.receive(plugin, commandBlock);
        }

        for(Player player : players)    {
            player.addPotionEffect(new PotionEffect(type, duration * 20, stenght));
        }
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();
        map.put("parameters", getParameters());
        return map;
    }

    public static AddPotionAction deserialize(Map<String, Object> map)  {
        AddPotionAction addPotionAction = new AddPotionAction();
        addPotionAction.setParameters((List<Parameter>) map.get("parameters"));
        return addPotionAction;
    }
}
