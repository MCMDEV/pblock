package de.mcmdev.pblock.api.impl.inventories;

import de.mcmdev.pblock.PBlock;
import de.mcmdev.pblock.api.Action;
import de.mcmdev.pblock.api.CommandBlock;
import de.mcmdev.pblock.api.Parameter;
import de.mcmdev.pblock.utils.ItemStackBuilder;
import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.InventoryListener;
import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

import java.util.Map;
import java.util.function.Consumer;

public class ActionSetInv {

    private PBlock plugin;
    private SmartInventory parent;
    private CommandBlock commandBlock;

    public ActionSetInv(PBlock plugin, SmartInventory parent, CommandBlock commandBlock) {
        this.plugin = plugin;
        this.parent = parent;
        this.commandBlock = commandBlock;

        load();
    }

    private SmartInventory inventory;
    private CommandBlock backupState;

    private void load() {
        backupState = commandBlock;
        inventory = SmartInventory.builder()
                .title("§3Aktion festlegen")
                .manager(plugin.getManager())
                .size(6, 9)
                .id("pblock_actionset")
                .provider(new InventoryProvider() {
                    @Override
                    public void init(Player player, InventoryContents contents) {
                        for(Action action : plugin.getActionRegistry().getActions())    {
                            ItemStackBuilder builder = ItemStackBuilder.create(action.getMaterial())
                                    .withName(action.getName())
                                    .withLore("§7Benötige Parameter");

                            for(Map.Entry<String, Parameter> parameterEntry : action.getNamedParameters().entrySet())   {
                                if(parameterEntry.getKey().startsWith("_")) {
                                    builder.addLore("§e➥ §7" + parameterEntry.getValue().getName());
                                }   else    {
                                    builder.addLore("§e➥ §7" + parameterEntry.getValue().getName() + " §e(" + parameterEntry.getKey() + ")");
                                }
                            }

                            contents.add(ClickableItem.of(builder.build(), new Consumer<InventoryClickEvent>() {
                                @Override
                                public void accept(InventoryClickEvent event) {
                                    commandBlock.setAction(createAction(action));
                                    CommandBlockInv commandBlockInv = new CommandBlockInv(plugin, player, commandBlock);
                                    commandBlockInv.getInventory().open(player);
                                }
                            }));
                        }
                    }

                    @Override
                    public void update(Player player, InventoryContents contents) {

                    }
                }).listener(new InventoryListener<>(InventoryCloseEvent.class, new Consumer<InventoryCloseEvent>() {
                    @Override
                    public void accept(InventoryCloseEvent inventoryCloseEvent) {
                        commandBlock = backupState;
                    }
                })).build();
    }

    public SmartInventory getInventory() {
        return inventory;
    }

    private Action createAction(Action action)    {
        Class<? extends Action> actionClass = action.getClass();
        try {
            Action newAction = actionClass.newInstance();
            return newAction;
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
