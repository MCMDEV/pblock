package de.mcmdev.pblock.api.impl.parameters;

import org.bukkit.entity.EntityType;

public class PlayersType {

    private String typeId;
    private String name;
    private String specifiedPlayer;

    public PlayersType(String typeId, String name) {
        this.typeId = typeId;
        this.name = name;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecifiedPlayer() {
        return specifiedPlayer;
    }

    public void setSpecifiedPlayer(String specifiedPlayer) {
        this.specifiedPlayer = specifiedPlayer;
    }
}
