package de.mcmdev.pblock.api.impl.parameters;

import de.mcmdev.pblock.PBlock;
import de.mcmdev.pblock.api.CommandBlock;
import de.mcmdev.pblock.api.Parameter;
import de.mcmdev.pblock.api.impl.inventories.parameters.CreatureSelectionInv;
import de.mcmdev.pblock.utils.ItemStackBuilder;
import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;
import net.wesjd.anvilgui.AnvilGUI;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class MobsParameter extends Parameter<MobsType> implements ConfigurationSerializable {

    public MobsParameter() {
        super("§aKreaturen", Material.DISPENSER);
    }

    @Override
    public String getObjectString() {
        if (getObject().getTypeId().equals("type_mobs")) {
            return getObject().getEntityType().getName();
        } else {
            return getObject().getName();
        }
    }

    private MobsParameter instance = this;

    @Override
    public void openInGui(Player player, SmartInventory parent, PBlock plugin) {
        SmartInventory inventory = SmartInventory.builder()
                .id("pblock_parameter_entities")
                .type(InventoryType.HOPPER)
                .manager(plugin.getManager())
                .title("§3Entitäten auswählen")
                .provider(new InventoryProvider() {
                    @Override
                    public void init(Player player, InventoryContents contents) {
                        contents.add(ClickableItem.of(

                                ItemStackBuilder.create(Material.STAINED_GLASS_PANE)
                                        .withColor(ItemStackBuilder.BlockColor.BLUE)
                                        .withName("§9Alle Kreaturen")
                                        .build()

                                , new Consumer<InventoryClickEvent>() {
                                    @Override
                                    public void accept(InventoryClickEvent event) {
                                        setObject(new MobsType("all_mobs", "Alle Kreaturen"));
                                        parent.open(player);
                                    }
                                }));

                        contents.add(ClickableItem.of(

                                ItemStackBuilder.create(Material.STAINED_GLASS_PANE)
                                        .withColor(ItemStackBuilder.BlockColor.BLUE)
                                        .withName("§9Festgelegter Kreaturentyp")
                                        .build()

                                , new Consumer<InventoryClickEvent>() {
                                    @Override
                                    public void accept(InventoryClickEvent event) {
                                        CreatureSelectionInv creatureSelectionInv = new CreatureSelectionInv(plugin, parent, player, instance);
                                    }
                                }));

                    }

                    @Override
                    public void update(Player player, InventoryContents contents) {

                    }
                }).build();
        inventory.open(player);
    }

    @Override
    public List<Entity> receive(PBlock plugin, CommandBlock commandBlock) {
        MobsType type = getObject();
        List<Entity> all = (List<Entity>) commandBlock.getLocation().getWorld().getEntities();
        if (type.getTypeId().equals("all_mobs")) {
            return all;
        } else if (type.getTypeId().equals("type_mobs")) {
            return all.stream().filter(entity -> entity.getType().equals(getObject().getEntityType())).collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();
        map.put("typeId", getObject().getTypeId());
        map.put("name", getObject().getName());
        if (getObject().getEntityType() != null) {
            map.put("entityType", getObject().getEntityType().name());
        }
        return map;
    }

    public static MobsParameter deserialize(Map<String, Object> map) {
        MobsParameter mobsParameter = new MobsParameter();
        MobsType mobsType = new MobsType((String) map.get("typeId"), (String) map.get("name"));
        if (map.containsKey("entityType")) {
            mobsType.setEntityType(EntityType.valueOf((String) map.get("entityType")));
        }
        mobsParameter.setObject(mobsType);
        return mobsParameter;
    }
}
