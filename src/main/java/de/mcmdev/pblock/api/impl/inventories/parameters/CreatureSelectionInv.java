package de.mcmdev.pblock.api.impl.inventories.parameters;

import de.mcmdev.pblock.PBlock;
import de.mcmdev.pblock.api.CommandBlock;
import de.mcmdev.pblock.api.Parameter;
import de.mcmdev.pblock.api.impl.parameters.MobsParameter;
import de.mcmdev.pblock.api.impl.parameters.MobsType;
import de.mcmdev.pblock.utils.ItemStackBuilder;
import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

import java.util.function.Consumer;

public class CreatureSelectionInv {

    private PBlock plugin;
    private SmartInventory parent;
    private Player player;
    private MobsParameter parameter;

    public CreatureSelectionInv(PBlock plugin, SmartInventory parent, Player player, MobsParameter parameter) {
        this.plugin = plugin;
        this.parent = parent;
        this.player = player;
        this.parameter = parameter;

        load();
    }

    private SmartInventory inventory;

    public void load() {
        inventory = SmartInventory.builder()
                .id("pblock_parameter_creature")
                .manager(plugin.getManager())
                .size(6, 9)
                .title("§3Kreaturentyp auswählen")
                .provider(new InventoryProvider() {
                    @Override
                    public void init(Player player, InventoryContents contents) {
                        for (EntityType entityType : EntityType.values()) {
                            contents.add(ClickableItem.of(

                                    ItemStackBuilder.create(Material.MONSTER_EGG)
                                            .withDamage((byte) entityType.getTypeId())
                                            .withName("§e" + entityType.getName())
                                            .build()

                                    , event -> {
                                        System.out.println(parent.toString());
                                        MobsType type = new MobsType("type_mobs", entityType.getName());
                                        type.setEntityType(entityType);
                                        parameter.setObject(type);
                                        parent.open(player);
                                    }));
                        }
                    }

                    @Override
                    public void update(Player player, InventoryContents contents) {

                    }
                })
                .build();
        inventory.open(player);

    }

    private int colorFromName(String name) {
        int i = Integer.parseInt(Integer.toString(name.hashCode()).substring(0, 1));
        if (i == 7 || i == 8) {
            i += 4;
        }
        i++;
        return i;
    }

    public SmartInventory getInventory() {
        return inventory;
    }
}
