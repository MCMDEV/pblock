package de.mcmdev.pblock.api.impl.parameters;

import de.mcmdev.pblock.PBlock;
import de.mcmdev.pblock.api.CommandBlock;
import de.mcmdev.pblock.api.Parameter;
import de.mcmdev.pblock.api.impl.actions.TeleportAction;
import de.mcmdev.pblock.utils.ItemStackBuilder;
import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;
import net.wesjd.anvilgui.AnvilGUI;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;

import java.util.*;
import java.util.function.Consumer;

public class PlayersParameter extends Parameter<PlayersType> implements ConfigurationSerializable {

    public PlayersParameter() {
        super("§eSpieler", Material.DISPENSER);
    }

    @Override
    public String getObjectString() {
        if(getObject().getTypeId().equals("specific_player"))    {
            return getObject().getSpecifiedPlayer();
        }   else    {
            return getObject().getName();
        }
    }

    @Override
    public void openInGui(Player player, SmartInventory parent, PBlock plugin) {
        SmartInventory inventory = SmartInventory.builder()
                .id("pblock_parameter_entities")
                .type(InventoryType.HOPPER)
                .manager(plugin.getManager())
                .title("§3Entitäten auswählen")
                .provider(new InventoryProvider() {
                    @Override
                    public void init(Player player, InventoryContents contents) {
                        contents.add(ClickableItem.of(

                                ItemStackBuilder.create(Material.STAINED_GLASS_PANE)
                                        .withColor(ItemStackBuilder.BlockColor.BLUE)
                                        .withName("§9Alle Spieler")
                                        .build()

                                , new Consumer<InventoryClickEvent>() {
                                    @Override
                                    public void accept(InventoryClickEvent event) {
                                        setObject(new PlayersType("all_players", "Alle Spieler"));
                                        parent.open(player);
                                    }
                                }));

                        contents.add(ClickableItem.of(

                                ItemStackBuilder.create(Material.STAINED_GLASS_PANE)
                                        .withColor(ItemStackBuilder.BlockColor.BLUE)
                                        .withName("§9Zufälliger Spieler")
                                        .build()

                                , new Consumer<InventoryClickEvent>() {
                                    @Override
                                    public void accept(InventoryClickEvent event) {
                                        setObject(new PlayersType("random_player", "Zufälliger Spieler"));
                                        parent.open(player);
                                    }
                                }));

                        contents.add(ClickableItem.of(

                                ItemStackBuilder.create(Material.STAINED_GLASS_PANE)
                                        .withColor(ItemStackBuilder.BlockColor.BLUE)
                                        .withName("§9Nächster Spieler")
                                        .build()

                                , new Consumer<InventoryClickEvent>() {
                                    @Override
                                    public void accept(InventoryClickEvent event) {
                                        setObject(new PlayersType("nearest_player", "Nächster Spieler"));
                                        parent.open(player);
                                    }
                                }));

                        contents.add(ClickableItem.of(

                                ItemStackBuilder.create(Material.STAINED_GLASS_PANE)
                                        .withColor(ItemStackBuilder.BlockColor.BLUE)
                                        .withName("§9Festgelegter Spieler")
                                        .build()

                                , new Consumer<InventoryClickEvent>() {
                                    @Override
                                    public void accept(InventoryClickEvent event) {
                                        new AnvilGUI.Builder()
                                                .text("Spielernamen eingeben")
                                                .onComplete((player1, s) -> {
                                                    String name = s;
                                                    PlayersType type = new PlayersType("specific_player", "Nächster Spieler");
                                                    type.setSpecifiedPlayer(name);
                                                    setObject(type);
                                                    parent.open(player);
                                                    return AnvilGUI.Response.close();
                                                })
                                                .plugin(plugin)
                                                .open(player);
                                    }
                                }));
                    }

                    @Override
                    public void update(Player player, InventoryContents contents) {

                    }
                }).build();
        inventory.open(player);
    }

    @Override
    public List<Player> receive(PBlock plugin, CommandBlock commandBlock) {
        PlayersType type = getObject();
        List<Player> all = (List<Player>) plugin.getServer().getOnlinePlayers();
        if(type.getTypeId().equals("all_players"))   {
            return all;
        }   else
        if(type.getTypeId().equals("random_player"))   {
            int id = new Random().nextInt(all.size());
            return Collections.singletonList(all.get(id));
        }   else
        if(type.getTypeId().equals("nearest_player"))   {
            Optional<Player> player = getNearest(commandBlock.getLocation(), all);
            if(player.isPresent())  {
                return Collections.singletonList(player.get());
            }
        }   else
        if(type.getTypeId().equals("specific_player"))   {
            String name = getObject().getSpecifiedPlayer();
            Player player = plugin.getServer().getPlayerExact(name);
            if(player != null && player.isOnline()) {
                return Collections.singletonList(player);
            }
        }
        return null;
    }


    private Optional<Player> getNearest(Location start, List<Player> all)   {
        Player currentNearest = null;
        double currentNearestRange = 200;

        for(Player player : all)    {
            if(start.distanceSquared(player.getLocation()) < currentNearestRange)   {
                currentNearest = player;
                currentNearestRange = start.distanceSquared(player.getLocation());
            }
        }
        return Optional.ofNullable(currentNearest);
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();
        map.put("typeId", getObject().getTypeId());
        map.put("name", getObject().getName());
        if(getObject().getSpecifiedPlayer() != null)    {
            map.put("specified", getObject().getSpecifiedPlayer());
        }
        return map;
    }

    public static PlayersParameter deserialize(Map<String, Object> map)  {
        PlayersParameter playersParameter = new PlayersParameter();
        PlayersType playersType = new PlayersType((String) map.get("typeId"), (String) map.get("name"));
        if(map.containsKey("specified"))    {
            playersType.setSpecifiedPlayer((String) map.get("specified"));
        }
        playersParameter.setObject(playersType);
        return playersParameter;
    }
}
