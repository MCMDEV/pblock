package de.mcmdev.pblock.api.impl.parameters;

import de.mcmdev.pblock.PBlock;
import de.mcmdev.pblock.api.CommandBlock;
import de.mcmdev.pblock.api.Parameter;
import de.mcmdev.pblock.utils.ItemStackBuilder;
import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;
import org.bukkit.Material;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.potion.PotionEffectType;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class EffectParameter extends Parameter<PotionEffectType> implements ConfigurationSerializable {
    public EffectParameter() {
        super("§ePotion Effect", Material.BREAD);
    }

    @Override
    public String getObjectString() {
        return getObject().getName();
    }

    @Override
    public void openInGui(Player player, SmartInventory parent, PBlock plugin) {
        SmartInventory inventory = SmartInventory.builder()
                .id("pblock_parameter_effect")
                .size(6, 9)
                .manager(plugin.getManager())
                .title("§dTrankeffekt auswählen")
                .provider(new InventoryProvider() {
                    @Override
                    public void init(Player player, InventoryContents contents) {
                        for(PotionEffectType type : PotionEffectType.values())  {
                            if(type == null) continue;
                            contents.add(ClickableItem.of(
                                    ItemStackBuilder.create(Material.GLASS_BOTTLE)
                                            .withName("§a" + type.getName())
                                            .build()
                                    , new Consumer<InventoryClickEvent>() {
                                        @Override
                                        public void accept(InventoryClickEvent event) {
                                            setObject(type);
                                            event.setCursor(null);
                                            parent.open(player);
                                        }
                                    }));
                        }
                    }

                    @Override
                    public void update(Player player, InventoryContents contents) {

                    }
                }).build();
        inventory.open(player);
    }

    @Override
    public Object receive(PBlock plugin, CommandBlock commandBlock) {
        return getObject();
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();
        map.put("potionEffectType", getObject().getName());
        return map;
    }

    public static EffectParameter deserialize(Map<String, Object> map)  {
        EffectParameter effectParameter = new EffectParameter();
        effectParameter.setObject(PotionEffectType.getByName(String.valueOf(map.get("potionEffectType"))));
        return effectParameter;
    }
}
